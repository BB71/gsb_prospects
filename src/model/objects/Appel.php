<?php
/**
 * File :        Praticien.php
 * Location :    gsb_prospects/src/model/objects/Praticien.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\objects;

use gsb_prospects\model\dao\VilleDAO;
use gsb_prospects\model\objects\Ville;
use gsb_prospects\model\objects\TypePraticien;

/**
 * Class Praticien
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
class Appel extends AbstractObject
{
    /**
     * Properties
     *
     * @var int    $id
     * @var string $nom
     * @var string $sujet
     * @var object $leTypePraticien
     */
    protected $id;
    protected $nom;
    protected $sujet;
    protected $id_Type_Praticien;
    protected $leTypePraticien;

    /* Methods */

    /**
     * __construct
     *
     * @param int    $id                id
     * @param string $nom               nom
     * @param string $prenom            prenom
     * @param string $adresse           adresse
     * @param int    $id_Ville          id_Ville (default:null)
     * @param int    $id_Type_Praticien id_Type_Praticien (default:null)
     */
    public function __construct($id, $nom, $sujet, $id_Type_Praticien = null)
    {
        $this->id                = $id;
        $this->nom               = $nom;
        $this->sujet            = $sujet;
        $this->id_Type_Praticien = $id_Type_Praticien;
        $this->leTypePraticien   = null;
    }

    /**
     * Function getId
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Procedure setId
     *
     * @param string $value value
     *
     * @return void
     */
    public function setId(string $value)
    {
        $this->id = $value;
    }
 
    /**
     * Function getNom
     * 
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Procedure setNom
     *
     * @param string $value value
     *
     * @return void
     */
    public function setNom(string $value)
    {
        $this->nom = $value;
    }

    /**
     * Function getSujet
     *
     * @return string
     */
    public function getSujet(): string
    {
        return $this->sujet;
    }

    /**
     * Procedure setSuejt
     *
     * @param string $value value
     *
     * @return void
     */
    public function setSujet(string $value)
    {
        $this->sujet = $value;
    }


    /**
     * Function getIdTypePraticien
     *
     * @return int
     */
    public function getIdTypePraticien()
    {
        return $this->id_Type_Praticien;
    }
}